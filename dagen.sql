-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Gegenereerd op: 09 jan 2017 om 11:39
-- Serverversie: 10.1.9-MariaDB
-- PHP-versie: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tijdschema`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `dagen`
--

CREATE TABLE `dagen` (
  `dagen_id` int(11) NOT NULL,
  `dagen_vrij` int(11) NOT NULL,
  `dagen_aantal` int(11) NOT NULL,
  `dagen_datum` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `dagen`
--

INSERT INTO `dagen` (`dagen_id`, `dagen_vrij`, `dagen_aantal`, `dagen_datum`) VALUES
(1, 14, 1, '2017-01-09 10:38:29');

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `dagen`
--
ALTER TABLE `dagen`
  ADD PRIMARY KEY (`dagen_id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `dagen`
--
ALTER TABLE `dagen`
  MODIFY `dagen_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
