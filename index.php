<?php
include "connect.php";
$date =  date('D d h:i:s A');
if(isset($_POST['submit'])){
	$id = $_POST['vrij'];
	
	$stmt = $conn->prepare('SELECT * FROM dagen');
	$stmt->execute();
	
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		$dagen_vrij = $row['dagen_vrij'];
	}
	
	$stop = 1;
	
	if(!is_numeric($id)){
		$error =  "Je moet wel een nummer invullen!";
		$stop = 0;
	}elseif($dagen_vrij <= 0){
		$error = "Je kan geen vrije dagen meer opnemen!";
		$stop = 1;
	}elseif($id > $dagen_vrij){
		$error = "Te veel dagen om op te nemen!";
		$stop = 1;
	}elseif($stop == 0) {
		$error = "Error: an unknown error has occurred!";
	}else{
		$stmt = $conn->prepare('INSERT INTO dagen_vrij(dagenvrij_aantal,dagenvrij_dag) values (:id,:dag)');
		$stmt->bindParam(':id', $id);
		$stmt->bindParam(':dag', $date);
		$stmt->execute();
		
		$stmt = $conn->prepare('UPDATE dagen SET dagen_vrij = dagen_vrij - :id WHERE dagen_id = 1');
		$stmt->bindParam(':id', $id);
		$stmt->execute();
	}
}
if(isset($_POST['button'])){
	$stmt = $conn->prepare('SELECT COUNT(*) AS countrows FROM dagen');
	$stmt->execute();
	
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		$rowcount = $row['countrows'];
	}
	
	if($rowcount > 0){
		$stmt = $conn->prepare('UPDATE dagen SET dagen_aantal = dagen_aantal + 1, dagen_datum = :datum');
		$stmt->bindParam(':datum', $date);
		$stmt->execute();
	} else {
		$stmt = $conn->prepare('INSERT INTO dagen(dagen_aantal, dagen_datum) VALUES (1, :datum) ');
		$stmt->bindParam(':datum', $date);
		$stmt->execute();
	}
}
	$stmt = $conn->prepare('SELECT * FROM dagen');
	$stmt->execute();
	
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		$dagen = $row['dagen_aantal'];
		$datum = $row['dagen_datum'];
		$vrij = $row['dagen_vrij'];
	}
		
	$d = 94 - $dagen;
	
?>
<html>
<head>
	<title>Tijdregistratie systeem voor stage</title>
</head>
<body>
	<h1>Tijdregistratie systeem voor stage</h1>
	<?php
		echo "Stage periode is van 30 januari tot en met 30 juni 2017 dat zijn ~108 dagen.</br>";
		echo "Dagen gewerkt: " . $dagen . "</br>";
		echo "Nog te doen: " . $d . "</br>";
		echo "Voor het laatst bijgewerkt op: <b>" . $datum . "</b></br>";
		if(isset($_POST['button'])){
			echo "Nieuwe dag toegevoegd!";
		} else {
	?>
	<form method="post" action="">
		<input type="submit" name="button" value="Weer een dag gewerkt!"></input>
	</form>
	<!--
<h1>Vrij nemen vul hier de dagen in:</h1>
	<?php
		$stmt = $conn->prepare('SELECT * FROM dagen_vrij');
		$stmt->execute();
		
		while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			$dagen_vrijgenomen = $row['dagenvrij_dag'];
			$dagen_vrijaantal = $row['dagenvrij_aantal'];
		}
		
		echo (isset($error)) ? $error . '</br>' : '';
		echo "Hoeveel dagen kan je nog vrij nemen: <b>" . $vrij . " </b></br>";
		echo "Welke dagen heb je al vrij genomen: </br>" . $dagen_vrijgenomen . "</br>";
		
	?>
	<form method="post" action="">
		<input type="text" name="vrij" style="width: 50px;"></input>
		<input type="submit" name="submit" value="Neem vrij!"></input>
	</form>
-->
	<?php		
		
	}
	?>
</body>
</html>